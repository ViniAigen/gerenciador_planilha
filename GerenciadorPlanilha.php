<?php 
namespace Aigen\TemaFesta;


class GerenciadorPlanilha
  {
	private $planilha = null;	
	private $planilha_uri = __DIR__ . '/base/planilha.xlsx';	
	private $planilha_formato = ['entrada' => 'Xlsx', 'saida' => 'Xls'];
	private $planilha_path;
	private $tipos_e_cores = null;
	private $tipos_e_cores_config = 
	  [
	  	[
	  		'titulo' => 7,
	  		'cores' => 
	  		  [
	  		  	'inicio' => 8, 
	  		  	'termino' => 32
	  		  ]
	  	],
	  	[
	  		'titulo' => 33,
	  		'cores' => 
	  		  [
	  		  	'inicio' => 34, 
	  		  	'termino' => 61
	  		  ]
	  	],
	  	[
	  		'titulo' => 68,
	  		'cores' => 
	  		  [
	  		  	'inicio' => 69,
	  		  	'termino' => 80
	  		  ]
	  	],
	  	[
	  		'titulo' => 81,
	  		'cores' => 
	  		  [
	  		  	'inicio' => 83,
	  		  	'termino' => 105
	  		  ]
	  	],
	  ];
	private $categorias = null;
	private $categorias_config =
	  [
	  	[
	  	  'titulo' => 'B',
	  	  'tamanhos' =>
	  	    [
	  	    	'B', 'C', 'D'
	  	    ]
	  	],
	  	[
	  	  'titulo' => 'E',
	  	  'tamanhos' =>
	  	    [
	  	    	'E', 'F', 'G', 'H'
	  	    ]
	  	],
	  	[
	  	  'titulo' => 'I',
	  	  'tamanhos' =>
	  	    [
	  	    	'I', 'J'
	  	    ]
	  	],
	  	[
	  	  'titulo' => 'K',
	  	  'tamanhos' =>
	  	    [
	  	    	'K', 'L'
	  	    ]
	  	],
	  	[
	  	  'titulo' => 'M',
	  	],
	  	[
	  	  'titulo' => 'N',
	  	],
	  	[
	  	  'titulo' => 'O',
	  	],
	  	[
	  	  'titulo' => 'P',
	  	  'tamanhos' => 
	  	    [
	  	    	'P', 'Q'
	  	    ]
	  	],
	  	[
	  	  'titulo' => 'R',
	  	],
	  	[
	  	  'titulo' => 'S',
	  	  'tamanhos' => 
	  	    [
	  	    	'S', 'T'
	  	    ]
	  	],
	  	[
	  		'titulo' => 'U'
	  	],
	  	[
	  		'titulo' => 'V'
	  	],
	  	[
	  		'titulo' => 'W'
	  	],
	  	[
	  		'titulo' => 'X'
	  	],
	  	[
	  		'titulo' => 'Y'
	  	],
	  	[
	  		'titulo' => 'Z'
	  	],
	  	[
	  		'titulo' => 'AA'
	  	],
	  	[
	  	  'titulo'   => 'AB',
	  	  'tamanhos' => 
	  	    [
	  	    	'AB', 'AC'
	  	    ]
	  	],
	  	[
	  		'titulo' => 'AD'
	  	]
	  ];
	private $categorias_especiais = null;
	private $categorias_especiais_config = 
	  [
	  	'categorias_tipo_2' =>
	  	  [
	  	  	'config' =>
	  	  	  [
	  	  	  	'titulos'           => ['E', 'M'],
	  	  	  	'valores_unitarios' => ['I', 'Q'],
	  	  	  	'quantidades'       => ['H', 'P']
	  	  	  ],
	  	  	'categorias' =>
	  	  	  [
	  	  	  	[
	  	  	  		[
	  	  	  			'inicio' => 110,
	  	  	  			'termino' => 119
	  	  	  		],
	  	  	  		[
	  	  	  			'inicio' => 122,
	  	  	  			'termino' => 132
	  	  	  		],
	  	  	  		[
	  	  	  			'inicio' => 135,
	  	  	  			'termino' => 144
	  	  	  		],
	  	  	  		[
	  	  	  			'inicio' => 147,
	  	  	  			'termino' => 156
	  	  	  		],
	  	  	  		[
	  	  	  			'inicio' => 160,
	  	  	  			'termino' => 168
	  	  	  		],
	  	  	  		[
	  	  	  			'inicio' => 171,
	  	  	  			'termino' => 180
	  	  	  		],
	  	  	  	],
	  	  	  	[
	  	  	  		[
	  	  	  			'inicio' => 110,
	  	  	  			'termino' => 120
	  	  	  		],
	  	  	  		[
	  	  	  			'inicio' => 123,
	  	  	  			'termino' => 130
	  	  	  		],
	  	  	  		[
	  	  	  			'inicio' => 133,
	  	  	  			'termino' => 147
	  	  	  		],
	  	  	  		[
	  	  	  			'inicio' => 150,
	  	  	  			'termino' => 164
	  	  	  		],
	  	  	  	]
	  	  	  ]
	  	  ],
	  	'etiquetas' =>
	  	  [
	  	  	'colunas' =>
	  	  	  [
	  	  	  	'titulos'     => 'A',
	  	  	  	'quantidades' => 'B'
	  	  	  ],
	  	  	'linhas' =>
	  	  	  [
	  	  	  	'inicio'  => 110,
	  	  	  	'termino' => 126
	  	  	  ],
	  	  	'valor_unitario' => 
	  	  	  [
	  	  	  	'coluna' => 'B',
	  	  	  	'linha'  => 128
	  	  	  ]
	  	  ],
	  ];
	function __construct($planilha_path = __DIR__ . '/pedidos/teste.xlsx')
	  {
	  	$permissoes = substr(sprintf('%o', fileperms(__DIR__)), -4);
	  	if($permissoes !== "0777")
	  	  {
	  	  	throw new \Exception('Garanta todas as permissões na pasta dessa classe', 500);
	  	  }
	  	$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($this->planilha_formato['entrada']);
		$list = $reader->listWorksheetInfo($this->planilha_uri);
		$name = $list[0]['worksheetName'];
		$reader->setLoadSheetsOnly($name);
		$planilha =  @$reader->load($this->planilha_uri);
		$this->planilha = $planilha->getActiveSheet();
		$this->planilha_path = $planilha_path;
	  }
	private function valorCelula($coluna, $linha)
	  {
	  	$valor = $this->planilha->getCell($coluna . $linha)->getValue();
	  	if(
	  		(is_object($valor))
	  		&&
	  		(get_class($valor) === 'PhpOffice\PhpSpreadsheet\RichText\RichText')
	  	  )
	  	  {
	  		$valor = $valor->getPlainText();
	  	  }
	  	return trim($valor);
	  }
	private function inserirValorCelula($valor, $coluna, $linha, &$planilha)
	  {
	  	$planilha->getCell($coluna . $linha)->setValue($valor);
	  }
	private function gerarUriPedido()
	  {
	  	return $this->planilha_path;
	  }
	private function removerAcentos($string)
	  {
  	    if ( !preg_match('/[\x80-\xff]/', $string) )
  	        return $string;

  	    $chars = array(
  	    chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
  	    chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
  	    chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
  	    chr(195).chr(135) => 'C', chr(195).chr(136) => 'E',
  	    chr(195).chr(137) => 'E', chr(195).chr(138) => 'E',
  	    chr(195).chr(139) => 'E', chr(195).chr(140) => 'I',
  	    chr(195).chr(141) => 'I', chr(195).chr(142) => 'I',
  	    chr(195).chr(143) => 'I', chr(195).chr(145) => 'N',
  	    chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
  	    chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
  	    chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
  	    chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
  	    chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
  	    chr(195).chr(159) => 's', chr(195).chr(160) => 'a',
  	    chr(195).chr(161) => 'a', chr(195).chr(162) => 'a',
  	    chr(195).chr(163) => 'a', chr(195).chr(164) => 'a',
  	    chr(195).chr(165) => 'a', chr(195).chr(167) => 'c',
  	    chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
  	    chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
  	    chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
  	    chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
  	    chr(195).chr(177) => 'n', chr(195).chr(178) => 'o',
  	    chr(195).chr(179) => 'o', chr(195).chr(180) => 'o',
  	    chr(195).chr(181) => 'o', chr(195).chr(182) => 'o',
  	    chr(195).chr(182) => 'o', chr(195).chr(185) => 'u',
  	    chr(195).chr(186) => 'u', chr(195).chr(187) => 'u',
  	    chr(195).chr(188) => 'u', chr(195).chr(189) => 'y',
  	    chr(195).chr(191) => 'y',
  	    chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
  	    chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
  	    chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
  	    chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
  	    chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
  	    chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
  	    chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
  	    chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
  	    chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
  	    chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
  	    chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
  	    chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
  	    chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
  	    chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
  	    chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
  	    chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
  	    chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
  	    chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
  	    chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
  	    chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
  	    chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
  	    chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
  	    chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
  	    chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
  	    chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
  	    chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
  	    chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
  	    chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
  	    chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
  	    chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
  	    chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
  	    chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
  	    chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
  	    chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',
  	    chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
  	    chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',
  	    chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
  	    chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',
  	    chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
  	    chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',
  	    chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
  	    chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
  	    chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
  	    chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
  	    chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
  	    chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
  	    chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
  	    chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
  	    chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
  	    chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
  	    chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
  	    chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
  	    chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
  	    chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
  	    chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
  	    chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
  	    chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
  	    chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
  	    chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
  	    chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
  	    chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
  	    chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
  	    chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
  	    chr(197).chr(190) => 'z', chr(197).chr(191) => 's'
  	    );
  	    $string = strtr($string, $chars);
  	    return $string;
	  }
	private function ajeitarCaracteres($string)
	  {
	  	return str_replace([" ", ":"], "", mb_strtoupper($this->removerAcentos($string)));
	  }
	public function gerarTipos()
	  {
	  	if(!is_null($this->tipos_e_cores))
	  	  {
	  	  	return $this->tipos_e_cores;
	  	  }
	  	$tipos_e_cores = [];
	  	$coluna = 'A';
	  	foreach($this->tipos_e_cores_config as $cfg)
	  	  {
	  	  	$tipo = 
	  	  	  [
	  	  	  	'titulo' => $this->valorCelula($coluna, $cfg['titulo']),
	  	  	  	'opcoes' => []
	  	  	  ];
	  	  	for($i = $cfg['cores']['inicio']; $i <= $cfg['cores']['termino']; $i++)
	  	  	  {
	  	  	  	$valor = $this->valorCelula($coluna, $i);
	  	  	  	if($valor)
	  	  	  	  {
	  	  	  	  	array_push($tipo['opcoes'], 
	  	  	  	  	  [
	  	  	  	  	  	'titulo' => $valor, 
	  	  	  	  	  	'linha' => $i, 
	  	  	  	  	  	'coluna' => null
	  	  	  	  	  ]);
	  	  	  	  }
	  	  	  }
	  		array_push($tipos_e_cores, $tipo);
	  	  }
	  	$this->tipos_e_cores = $tipos_e_cores;
	  	return $tipos_e_cores;
	  }
	public function gerarCategorias()
	  {
	  	if(!is_null($this->categorias))
	  	  {
	  	  	return $this->categorias;
	  	  }
	  	if(!file_exists(__DIR__ . '/categorias.json'))
	  	  {
  	  	  	$tipos = $this->gerarTipos();
  	  	  	$categorias = [];
  	  	  	$linhas = (object) ['categorias' => 5, 'tamanhos' => 6, 'valores_unitarios' => 107];
  	  	  	foreach($this->categorias_config as $cfg)
  	  	  	  {
  	  	  	  	$categoria = 
  	  	  	  	  [
  	  	  	  	  	'titulo' => $this->valorCelula($cfg['titulo'], $linhas->categorias),
  	  	  	  	  	'tipo_categoria' => null,
  	  	  	  	  ];
  	  	  	  	if(isset($cfg['tamanhos']))
  	  	  	  	  {
  	  	  	  	  	$tamanhos = [];
  	  	  	  	  	$categoria['tipo_categoria'] = 0;
  	  	  	  	  	foreach($cfg['tamanhos'] as $tmo)
  	  	  	  	  	  {
  	  	  	  	  	  	$_tamanho = $this->valorCelula($tmo, $linhas->tamanhos);
  	  	  	  	  	  	if($_tamanho)
  	  	  	  	  	  	  {
  	  	  	  	  	  	  	$tamanho = 
  	  	  	  	  	  	  	  [
  	  	  	  	  	  	  	  	'titulo' => $_tamanho,
  	  	  	  	  	  	  	  	'valor_unitario' => 
  	  	  	  	  	  	  	  	  [
  	  	  	  	  	  	  	  	  	'valor' => $this->valorCelula($tmo, $linhas->valores_unitarios),
  	  	  	  	  	  	  	  	  	'coordenadas' =>
  	  	  	  	  	  	  	  	  	  [
  	  	  	  	  	  	  	  	  	  	'coluna' => $tmo,
  	  	  	  	  	  	  	  	  	  	'linha' => $linhas->valores_unitarios
  	  	  	  	  	  	  	  	  	  ]
  	  	  	  	  	  	  	  	  ],
  	  	  	  	  	  	  	  	'tipos' => $tipos
  	  	  	  	  	  	  	  ];
  	  	  	  	  	  	  	foreach($tamanho['tipos'] as &$_tipo)
  	  	  	  	  	  	  	  {
  	  	  	  	  	  	  	  	foreach($_tipo['opcoes'] as &$opcao)
  	  	  	  	  	  	  	  	  {
  	  	  	  	  	  	  	  	  	$opcao['coluna'] = $tmo;
  	  	  	  	  	  	  	  	  }
  	  	  	  	  	  	  	  }
  	  	  	  	  	  	  	array_push($tamanhos, $tamanho);
  	  	  	  	  	  	  }
  	  	  	  	  	  }
  	  	  	  	  	$categoria['tamanhos'] = $tamanhos;
  	  	  	  	  }
  	  	  	  	else
  	  	  	  	  {
  	  	  	  	  	$categoria['tipo_categoria'] = 1;
  	  	  	  	  	$categoria['titulo'] .= " " . $this->valorCelula($cfg['titulo'], $linhas->tamanhos);
  	  	  	  	  	$categoria['valor_unitario'] = 
  	  	  	  	  	  [
  	  	  	  	  	  	'valor' => $this->valorCelula($cfg['titulo'], $linhas->valores_unitarios),
  	  	  	  	  	  	'coordenadas' =>
  	  	  	  	  	  	  [
  	  	  	  	  	  	  	'coluna' => $cfg['titulo'],
  	  	  	  	  	  	  	'linha' => $linhas->valores_unitarios
  	  	  	  	  	  	  ]
  	  	  	  	  	  ];
  	  	  	  	  	$categoria['tipos'] = $tipos;
  	  	  	  	  	foreach($categoria['tipos'] as &$_tipo)
  	  	  	  	  	  {
  	  	  	  	  	  	foreach($_tipo['opcoes'] as &$opcao)
  	  	  	  	  	  	  {
  	  	  	  	  	  		$opcao['coluna'] = $cfg['titulo'];
  	  	  	  	  	  	  }
  	  	  	  	  	  }
  	  	  	  	  }
  	  	  	  	array_push($categorias, $categoria);
  	  	  	  }
  	  	  	$categoria_config = $this->categorias_especiais_config['categorias_tipo_2'];
  	  	  	foreach($categoria_config['categorias'] as $coluna_tabela => $config)
  	  	  	  {
  	  	  	  	foreach($config as $cfg)
  	  	  	  	  {
  	  	  	  	  	$categoria = 
  	  	  	  	  	  [
  	  	  	  	  	  	'titulo' => $this->valorCelula($categoria_config['config']['titulos'][$coluna_tabela], $cfg['inicio']),
  	  	  	  	  	  	'tipo_categoria' => 2,
  	  	  	  	  	  ];
  	  	  	  	  	$tipos = [];
  	  	  	  	  	for($i = ($cfg['inicio'] + 1); $i <= $cfg['termino']; $i++)
  	  	  	  	  	  {
  	  	  	  	  	  	$titulo = $this->valorCelula($categoria_config['config']['titulos'][$coluna_tabela], $i);
  	  	  	  	  	  	if(!empty($titulo))
  	  	  	  	  	  	  {
  	  		  	  	  	  	$tipo =
  	  		  	  	  	  	  [
  	  		  	  	  	  	  	'coluna' => $categoria_config['config']['quantidades'][$coluna_tabela],
  	  		  	  	  	  	  	'linha'  => $i,
  	  		  	  	  	  	  	'titulo' => $titulo,
  	  		  	  	  	  	  	'valor_unitario'  => 
  	  		  	  	  	  	  	  [
  	  		  	  	  	  	  	  	'valor' => $this->valorCelula($categoria_config['config']['valores_unitarios'][$coluna_tabela], $i),
  	  		  	  	  	  	  	  	'coordenadas' =>
  	  		  	  	  	  	  	  	  [
  	  		  	  	  	  	  	  	  	'coluna' => $categoria_config['config']['valores_unitarios'][$coluna_tabela],
  	  	  	  	  	  	  				'linha' => $i
  	  		  	  	  	  	  	  	  ]
  	  		  	  	  	  	  	  ],
  	  		  	  	  	  	  ];
  	  		  	  	  	  	array_push($tipos, $tipo);
  	  	  	  	  	  	  }
  	  	  	  	  	  }
  	  	  	  	  	$categoria['tipos'] = $tipos;
  	  	  	  	  	array_push($categorias, $categoria);
  	  	  	  	  }
  	  	  	  }
  	  	  	$categoria_config = $this->categorias_especiais_config['etiquetas'];
  	  	  	extract($categoria_config);
  	  	  	$categoria = 
  	  	  	  [
  	  	  	  	'titulo' => $this->valorCelula($colunas['titulos'], $linhas['inicio']),
  	  	  	  	'tipo_categoria' => 3,
  	  	  	  	'valor_unitario' => 
  	  	  	  	  [
  	  	  	  	  	'valor' => $this->valorCelula($valor_unitario['coluna'], $valor_unitario['linha']),
  	  	  	  	  	'coordenadas' =>
  	  	  	  	  	  [
  	  	  	  	  	  	'coluna' => $valor_unitario['coluna'],
  	  	  	  	  	  	'linha'  => $valor_unitario['linha'] 
  	  	  	  	  	  ]
  	  	  	  	  ]
  	  	  	  ];
  	  	  	$tipos = [];
  	  	  	for($i = ($linhas['inicio'] + 1); $i <= $linhas['termino']; $i++)
  	  	  	  {
  	  	  	  	$titulo = $this->valorCelula($colunas['titulos'], $i);
  	  	  	  	if(!empty($titulo))
  	  	  	  	  {
  	  	  	  	  	$tipo = 
  	  	  	  	  	  [
  	  	  	  	  	  	'coluna' => $colunas['quantidades'],
  	  	  	  	  	  	'linha'  => $i,
  	  	  	  	  	  	'titulo' => $titulo,
  	  	  	  	  	  ];
  	  	  	  	  	array_push($tipos, $tipo);
  	  	  	  	  }
  	  	  	  }
  	  	  	$categoria['tipos'] = $tipos;
  	  	  	array_push($categorias, $categoria);
  	  	  	file_put_contents(__DIR__ . '/categorias.json', json_encode($categorias));
	  	  }
	  	else
	  	  {
	  	  	$categorias = json_decode(file_get_contents(__DIR__ . '/categorias.json'), true);
	  	  }
	  	$this->categorias = $categorias;
	  	return $categorias;
	  }
	public function gerarCoordenadas()
	  {
	  	$categorias = $this->gerarCategorias();
	  	$coordenadas = [];
	  	foreach($categorias as $categoria)
	  	  {
	  	  	switch ($categoria['tipo_categoria'])
	  	  	  {
	  	  		case 0:
	  	  			$coordenadas[$this->ajeitarCaracteres($categoria['titulo'])] = [];
	  	  			foreach($categoria['tamanhos'] as $tamanho)
	  	  			  {
	  	  			  	$coordenadas[$this->ajeitarCaracteres($categoria['titulo'])][$this->ajeitarCaracteres($tamanho['titulo'])]['dados'] = [];
	  	  			  	$coordenadas[$this->ajeitarCaracteres($categoria['titulo'])][$this->ajeitarCaracteres($tamanho['titulo'])]['valor_unitario'] = $tamanho['valor_unitario']['coordenadas'];
	  	  			  	foreach($tamanho['tipos'] as $tipo)
	  	  			  	  {
	  	  			  	  	$coordenadas[$this->ajeitarCaracteres($categoria['titulo'])][$this->ajeitarCaracteres($tamanho['titulo'])]['dados'][$this->ajeitarCaracteres($tipo['titulo'])] = [];

	  	  			  	  	foreach($tipo['opcoes'] as $cor)
	  	  			  	  	  {
	  	  			  	  	  	$coordenadas[$this->ajeitarCaracteres($categoria['titulo'])][$this->ajeitarCaracteres($tamanho['titulo'])]['dados'][$this->ajeitarCaracteres($tipo['titulo'])][$this->ajeitarCaracteres($cor['titulo'])] = 
	  	  			  	  	  	  [
	  	  			  	  	  	  	'coluna' => $cor['coluna'],
	  	  			  	  	  	  	'linha' => $cor['linha']
	  	  			  	  	  	  ];
	  	  			  	  	  }
	  	  			  	  }
	  	  			  }
	  	  			break;
	  	  		case 1:
	  	  			$coordenadas[$this->ajeitarCaracteres($categoria['titulo'])] = [];
	  	  			$coordenadas[$this->ajeitarCaracteres($categoria['titulo'])]['valor_unitario'] = $categoria['valor_unitario']['coordenadas'];
	  	  			foreach($categoria['tipos'] as $tipo)
	  	  			  {
	  	  			  	$coordenadas[$this->ajeitarCaracteres($categoria['titulo'])]['dados'][$this->ajeitarCaracteres($tipo['titulo'])] = [];
	  	  			  	foreach($tipo['opcoes'] as $cores)
	  	  			  	  {
	  	  			  	  	$coordenadas[$this->ajeitarCaracteres($categoria['titulo'])]['dados'][$this->ajeitarCaracteres($tipo['titulo'])][$this->ajeitarCaracteres($cores['titulo'])] = 
	  	  			  	  	  [
	  	  			  	  	  	'linha' => $cores['linha'],
	  	  			  	  	  	'coluna' => $cores['coluna']
	  	  			  	  	  ];
	  	  			  	  }
	  	  			  }
	  	  			break;
	  	  		case 2: 
	  	  			$coordenadas[$this->ajeitarCaracteres($categoria['titulo'])] = [];
	  	  			foreach($categoria['tipos'] as $tipo)
	  	  			  {
	  	  			  	$coordenadas[$this->ajeitarCaracteres($categoria['titulo'])][$this->ajeitarCaracteres($tipo['titulo'])] = 
	  	  			  	  [
	  	  			  	  	'quantidade' => 
	  	  			  	  	  [
	  	  			  	  	  	'coluna' => $tipo['coluna'],
	  	  			  	  	  	'linha' => $tipo['linha']
	  	  			  	  	  ],
	  	  			  	  	'valor_unitario' => $tipo['valor_unitario']['coordenadas']
	  	  			  	  ];
	  	  			  }
	  	  			break;
	  	  		case 3: 
	  	  			$coordenadas[$this->ajeitarCaracteres($categoria['titulo'])] = [];
	  	  			$coordenadas[$this->ajeitarCaracteres($categoria['titulo'])]['valor_unitario'] = $categoria['valor_unitario']['coordenadas'];
	  	  			foreach($categoria['tipos'] as $tipo)
	  	  			  {
	  	  			  	$coordenadas[$this->ajeitarCaracteres($categoria['titulo'])]['dados'][$this->ajeitarCaracteres($tipo['titulo'])] = 
	  	  			  	  [
	  	  			  	  	'coluna' => $tipo['coluna'],
	  	  			  	  	'linha' => $tipo['linha']
	  	  			  	  ];
	  	  			  }
	  	  			break; 
	  	  	  }
	  	  }
	  	return $coordenadas;
	  }
	public function inserirPedido($data)
	  {
	  	//$spreadsheet->getActiveSheet()->getStyle($cells)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
	  	if(is_string($data))
	  	  {
	  	  	$data = json_decode($data, true);
	  	  }
	  	if(!is_array($data))
	  	  {
	  	  	throw new \Exception('Os dados não estão com o formato correto', 400);
	  	  }
	  	$data = json_decode(file_get_contents(__DIR__ . '/pedido-teste/pedido.json'), true);
	  	$coordenadas = $this->gerarCoordenadas();
	  	@$template = \PhpOffice\PhpSpreadsheet\IOFactory::load($this->planilha_uri);
	  	$planilha = $template->getActiveSheet();
	  	foreach($data as $categoria => $dados_categoria)
	  	  {
	  	  	switch ($dados_categoria['tipo_categoria']) 
	  	  	  {
	  	  		case 0:
	  	  			if(@count($dados_categoria['dados']))
	  	  			  {
	  	  			  	foreach($dados_categoria['dados'] as $tamanho => $tipos)
	  	  			  	  {
	  	  			  	  	$valor             = $tipos['valor_unitario'];
	  	  			  	  	$coordenadas_valor = $coordenadas[$this->ajeitarCaracteres($categoria)][$this->ajeitarCaracteres($tamanho)]['valor_unitario'];
	  	  			  	  	$this->inserirValorCelula($valor, $coordenadas_valor['coluna'], $coordenadas_valor['linha'], $planilha);
	  	  			  	  	unset($tipos['valor_unitario']);
	  	  			  	  	foreach($tipos as $tipo => $cores)
	  	  			  	  	  {
	  	  			  	  	  	foreach($cores as $cor => $quantidade)
	  	  			  	  	  	  {
	  	  			  	  	  	  	$coordenadas_quantidade = $coordenadas[$this->ajeitarCaracteres($categoria)][$this->ajeitarCaracteres($tamanho)]['dados'][$this->ajeitarCaracteres($tipo)][$this->ajeitarCaracteres($cor)];
									$this->inserirValorCelula($quantidade, $coordenadas_quantidade['coluna'], $coordenadas_quantidade['linha'], $planilha);	  	  			  	  	  	  	
	  	  			  	  	  	  }
	  	  			  	  	  }
	  	  			  	  }
	  	  			  }
	  	  			break;
	  	  		case 1:
	  	  			if(@count($dados_categoria['dados']))
	  	  			  {
	  	  			  	$valor = $dados_categoria['valor_unitario'];
	  	  			  	$coordenadas_valor = $coordenadas[$this->ajeitarCaracteres($categoria)]['valor_unitario'];
	  	  			  	$this->inserirValorCelula($valor, $coordenadas_valor['coluna'], $coordenadas_valor['linha'], $planilha);
	  	  			  	foreach($dados_categoria['dados'] as $tipos => $cores) 
	  	  			  	  {
	  	  			  		foreach($cores as $cor => $quantidade)
	  	  			  		  {
	  	  			  		  	$coordenadas_quantidade = $coordenadas[$this->ajeitarCaracteres($categoria)]['dados'][$this->ajeitarCaracteres($tipos)][$this->ajeitarCaracteres($cor)];
	  	  			  		  	$this->inserirValorCelula($quantidade, $coordenadas_quantidade['coluna'], $coordenadas_quantidade['linha'], $planilha);
	  	  			  		  }
	  	  			  	  }
	  	  			  }
					break;	
				case 2:
					if(@count($dados_categoria['dados']))
					  {
					  	foreach($dados_categoria['dados'] as $tipo => $dados)
					  	  {
					  	  	$coordenadas_valor      = $coordenadas[$this->ajeitarCaracteres($categoria)][$this->ajeitarCaracteres($tipo)]['valor_unitario'];
					  	  	$coordenadas_quantidade = $coordenadas[$this->ajeitarCaracteres($categoria)][$this->ajeitarCaracteres($tipo)]['quantidade'];
					  	  	$this->inserirValorCelula($dados['quantidade'], $coordenadas_quantidade['coluna'], $coordenadas_quantidade['linha'], $planilha);
					  	  	$this->inserirValorCelula($dados['valor_unitario'], $coordenadas_valor['coluna'], $coordenadas_valor['linha'], $planilha);
					  	  }
					  }
					break;  
				case 3:
					if(@count($dados_categoria['dados']))
					  {
					  	$valor = $dados_categoria['valor_unitario'];
					  	$coordenadas_valor = $coordenadas[$this->ajeitarCaracteres($categoria)]['valor_unitario'];
					  	$this->inserirValorCelula($valor, $coordenadas_valor['coluna'], $coordenadas_valor['linha'], $planilha);
					  	foreach($dados_categoria['dados'] as $tipo => $quantidade)
					  	  {
					  		$coordenadas_quantidade = $coordenadas[$this->ajeitarCaracteres($categoria)]['dados'][$this->ajeitarCaracteres($tipo)];
					  	  	$this->inserirValorCelula($quantidade, $coordenadas_quantidade['coluna'], $coordenadas_quantidade['linha'], $planilha);
					  	  }
					  }	  		
	  	  		default:
	  	  			# code...
	  	  			break;
	  	  	  }
	  	  }
	  	$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($template, $this->planilha_formato['saida']);
	  	$writer->save($this->gerarUriPedido());
	  }
  }
?>